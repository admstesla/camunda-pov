# Camunda PoV

## What's happening here?

The `docker-compose.yml` will deploy an instance of [Camunda (run)](https://docs.camunda.org/manual/7.15/user-guide/camunda-bpm-run/) and [Postgresql](https://www.postgresql.org/)

Check out [Run Caminda Platform user Docker](https://docs.camunda.org/manual/7.15/installation/docker/#start-camunda-bpm-run-using-docker) and the [PostgreSQL](https://hub.docker.com/_/postgres) docker hub page for more information

## Setup

1. clone this repo
2. create a `.env` in the working directory (see note below)
3. run

```shell
$ docker-compose up
```

If your `.env` file is not in the working directory, simply point to it as follows

```shell
$ docker-compose --env-file ./config/.env.dev up
```

## .env

I'm using a `.env` file to furnish [environment variable](https://docs.docker.com/compose/environment-variables/) in `docker-compose.yml` as follows:
```yaml
:${DISTRO:-latest}
```

In the `.env` file, simply add the following
```shell
DISTRO=run
```

## Persistent volumes

In the `docker-compose.yml` we are creating a volume to persist the database data

Identify the volume
```shell
$ docker volume ls
DRIVER    VOLUME NAME
local     camunda-pov_camunda-pgdata
```

Inspect the volume
```shell
$ docker volume inspect camunda-pov_camunda-pgdata
[
    {
        "CreatedAt": "2021-10-28T07:27:33+11:00",
        "Driver": "local",
        "Labels": {
            "com.docker.compose.project": "camunda-pov",
            "com.docker.compose.version": "1.27.4",
            "com.docker.compose.volume": "camunda-pgdata"
        },
        "Mountpoint": "/var/lib/docker/volumes/camunda-pov_camunda-pgdata/_data",
        "Name": "camunda-pov_camunda-pgdata",
        "Options": null,
        "Scope": "local"
    }
]
```
